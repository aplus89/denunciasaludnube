var api_key = 'key-d9c3c565dfdba18eda52bb580acdead4'
var domain = 'mg.denunciasalud.com'
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain})
var Mustache = require('mustache')
// Parse.Cloud.define('hello', function(req, res) {
//   res.success('Hi');
// });


function sendEmail (template, view, emails){
		
	var text = Mustache.render(template, view);
			
	

	var data = { from: 'Denuncia Salud <noresponder@denunciasalud.com>', 
		to: emails, 
		cc: 'denuncia.salud@misalud.go.cr', 
		subject: 'Nueva Denuncia',
		text: text
	}

	mailgun.messages().send(data, function (error, body) {
					console.log(body)
				})

}



Parse.Cloud.afterSave("Post", function(request) {

	//obtener correos cercanos

	var post = request.object
	var id = post.id
	
	// var sendToMS = post.get("ms")
	var name = post.get("name")

	// console.log(sendToMS)
	console.log(name != "")

	if(name != ""){
		
		var location = post.get("location")
		var description = post.get("description")
		var category = post.get("categoryName")
		var name = post.get("name")
		var confidentialBool = post.get("confidential")
		var email = post.get("email")
		var phone = post.get("phone")

		var confidential = confidentialBool ? "si" : "no"

		var view = {
			name: name,
			categoryName: category,
			confidential: confidential,
			email: email,
			phone: phone,
			id: id
		}

		var template = "Se ha realizado una denuncia cerca de su área rectora, con la siguiente información: \n \
			\n \
Denunciante: {{name}} \n \
Tipo de Denuncia: {{categoryName}} \n \
Denuncia confidencial: {{confidential}} \n \
Correo electrónico: {{email}} \n \
Teléfono: {{phone}} \n \
Ubicacion: http://www.denunciasalud.com/mapadenuncias.html?id={{id}} \n \
			\n \
Si esta denuncia es pertinente a su área rectora, favor contestar de recibido la denuncia."
		
		


		if (category != "atencion") {
		
			var postGeoPoint = location
			var AreasRectoras = Parse.Object.extend("AreasRectoras")

			var query = new Parse.Query(AreasRectoras)
			query.near("geo", postGeoPoint)
			query.limit(3)
			query.find({
				success: function(placesObjects) {

					var mails = []

					for (var i = 0; i < placesObjects.length; i++) {
						var place = placesObjects[i]
						var placeMails = place.get("emails")
						for (var j = 0; j < placeMails.length; j++) {
							var mail = placeMails[j]
							mails.push(mail)
						}
					}


					sendEmail (template, view, mails)

				}
			});

		}else { 

			sendEmail (template, view, ["contraloria.servicios@misalud.go.cr"])


		}

	}



})