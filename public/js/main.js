function Denuncia(parseObject){
	this.parseObject = parseObject
	this.lat = parseObject.get("location").latitude
	this.lng = parseObject.get("location").longitude
	this.text = parseObject.get("description")
	this.os = parseObject.get("os")
	this.videoUrl = "http://d2vpm3nruwfvqw.cloudfront.net/"+parseObject.id+".mp4"
	var self = this

	var perception = parseObject.get("perception")
	var category = parseObject.get("categoryName")

	this.categoryImg = "img/category/"+perception+"/"+category+".png"

	this.icon = {
	    url: "img/marker_"+perception+".png",
	    scaledSize: new google.maps.Size(15, 15),
	    origin: new google.maps.Point(0.5, 0.5)
	}

	this.selectMarker = function(){
		self.marker.setIcon(self.iconSelected)
	}

	this.deselectIcon = function () {
		self.marker.setIcon(self.icon)
	}

	this.iconSelected = {
	    url: "img/"+perception+".png",
	    scaledSize: new google.maps.Size(38.5, 40.5)
	}
}

var Post = Parse.Object.extend("Post")

var domain = ""

var app = {

	appId : "7Myg258h5XS4oi1n6sG4U4VIgu1bFIo0",
	serverURL: "http://appdenuncia.herokuapp.com/parse",
	$mapDiv: $("#map"),
	map: null,
	denuncias: [],
	currentDenuncia: null,
	sidebar: false,
	player: videojs('video_denuncia'),
	$postDescription: $("#sidebar h1"),
	$postCategory: $("#sidebar .postCategory"),
	init: function () {
		Parse.initialize(this.appId)
		Parse.serverURL = app.serverURL
		this.initMap()
		this.getDenuncias()
	},
	initCurrentDenuncia: function () {
		
		var idDenuncia = Url.queryString("id")

		if (idDenuncia != undefined) {

			app.centerAndShowDenuncia(app.findDenuncia(idDenuncia))

		}

		console.log(Url.getLocation())

	},
	findDenuncia: function (id) {
		for (var i = 0; i < app.denuncias.length; i++) {
			var denuncia = app.denuncias[i]

			if(denuncia.parseObject.id == id){

				return denuncia

			}

		}
	},
	getDenuncia: function(id){
		var query = new Parse.Query(Post)
		query.get(id, {
		  success: function(denuncia) {
			app.centerAndShowDenuncia(denuncia)
		  },
		  error: function(object, error) {
		    
		  }
		});
	},
	///Current
	centerAndShowDenuncia: function (denuncia) {

		if(denuncia != undefined){
			app.map.setCenter({lat: denuncia.lat, lng: denuncia.lng})

			app.showDenuncia(denuncia)

			app.map.setZoom(14)
		}
		

	},
	getDenuncias: function(){
		var query = new Parse.Query(Post)

		query.limit(1000)
		query.equalTo("active", true)
		query.find({
		  success: function(results) {
		  	console.log(results.length)
		    for (var i = 0; i < results.length; i++) {
				var denuncia = new Denuncia(results[i])

				var marker = new google.maps.Marker({
					position: {lat: denuncia.lat, lng: denuncia.lng},
					map: app.map,
					icon: denuncia.icon,
					title: denuncia.text
				})

				marker.denuncia = denuncia

				denuncia.marker = marker

				app.denuncias.push(denuncia)

				marker.addListener('click', function() {
					
					app.showDenuncia(this.denuncia)
					
				})
		    }


		    app.initCurrentDenuncia()


		  },
		  error: function(error) {
		    console.log("Error: " + error.code + " " + error.message);
		  }
		})
	},
	openSideBar: function () {
		// $("#map").width($("body").width()-300)
		// google.maps.event.trigger(app.map,'resize')
		$("#sidebar, #map").addClass('open-sidebar')
		app.sidebar = true
	},
	closeSideBar: function () {
		// $("#map").width($("body").width())
		// google.maps.event.trigger(app.map,'resize')
		$("#sidebar, #map").removeClass('open-sidebar')
		app.sidebar = false
	},
	setFacebookComments: function (denuncia) {
		console.log(Url.getLocation())
		$(".fb-comments").remove()
		$("#fb-comments").html('<div class="fb-comments" data-href="http://denunciasalud.com:1337'+Url.getLocation()+'" data-width="300" data-numposts="5"></div>')
		
		if (FB){
			FB.XFBML.parse(document.getElementById('fb-comments'))
		}

	},
	showDenuncia: function (denuncia) {

		if (app.currentDenuncia){ app.currentDenuncia.deselectIcon() }

		app.currentDenuncia = denuncia


		denuncia.selectMarker()

		Url.updateSearchParam("id", denuncia.parseObject.id)

		app.setFacebookComments(denuncia)


		if (!app.sidebar){
			app.openSideBar()
		}

		app.player.videoHeight(264)
		app.player.videoWidth(640)

		$(".video").removeAttr('class')
		.addClass('video').addClass(denuncia.os)

		app.player.src(denuncia.videoUrl)
		app.player.play()
		app.$postDescription.text(denuncia.text)
		app.$postCategory.attr('src', denuncia.categoryImg)
	},
	initMap: function () {
		this.map = new google.maps.Map(this.$mapDiv.get(0), {
		  	center: {lat: 9.9762753, lng: -84.1408462},
		  	zoom: 12,
		  	zoomControlOptions: {
        		position: google.maps.ControlPosition.LEFT_BOTTOM
    		},
    		streetViewControl: false
		})

		this.map.addListener('click', function() {
			Url.updateSearchParam("id")
			app.currentDenuncia.deselectIcon()	
			app.closeSideBar()
			app.player.pause()
		})
	}
}

function googleMapsCallBack(){
	$(function () {
		app.init()
	})
}
